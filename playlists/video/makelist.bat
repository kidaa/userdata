@echo off
setlocal enabledelayedexpansion

set PATH=%PATH%;D:\Users\sita\PortableApps\ConvertZ
set EXT=*.mkv *.mp4 *.avi *.rmvb *.rm *.iso *.mdf *.mpeg *.mpg *.wtv *.vob *.flv *.wmv *.mp3 *.m4a
REM set OUTPUT="%~dp0\playlist.m3u"
set _=%~dp0
set OUTPUT="%_:~0,-1%.m3u"
set OUTPUT2=%TEMP%\temp.m3u
REM set TMPFILE=%temp%\%~n0.temp.txt

:: save original codepage
for /f "tokens=2 delims=:" %%a in ('chcp') do @set /a "cp=%%~a"

pushd %~dp0
set SEARCH=%CD:~0,2%
set REPLACE=smb://%OUTPUT:~3,9%
for /f "tokens=1-2 delims=\ " %%i in (%OUTPUT%) do (
set REPLACE=smb://%%i\%%j
)
set REPLACE2=smb://%OUTPUT:~3,9%
for /f "tokens=1-2 delims=\ " %%i in (%OUTPUT%) do (
set REPLACE2=\\%%i\%%j
)

chcp 65001 >nul

cmd /u /c dir /b /s %EXT% >%OUTPUT%
ConvertZ.exe /i:ULE /o:utf8 %OUTPUT% %OUTPUT%
REM type %OUTPUT% | sort >%TMPFILE%
REM move %TMPFILE% %OUTPUT%
call :replace %OUTPUT% %SEARCH% %REPLACE%

cmd /u /c dir /b /s %EXT% >%OUTPUT2%
ConvertZ.exe /i:ULE /o:utf8 %OUTPUT2% %OUTPUT2%
call :replace %OUTPUT2% %SEARCH% %REPLACE2%
cat %OUTPUT2% >>%OUTPUT%
del %OUTPUT2%

chcp %cp% >nul

popd

REM pause
del %0

goto :EOF

:replace <file> <search string> <replace string>
set INTEXTFILE=%1
set OUTTEXTFILE=%temp%\%~n0.temp.txt
set SEARCHTEXT=%2
set REPLACETEXT=%3
set OUTPUTLINE=

REM echo 'type %INTEXTFILE%'
REM pause

for /f "tokens=1,* delims=?" %%A in ( 'type %INTEXTFILE%') do (

SET string=%%A
SET modified=!string:%SEARCHTEXT%=%REPLACETEXT%!

echo !modified! >> %OUTTEXTFILE%
)

echo.>>%OUTTEXTFILE%

del %INTEXTFILE%
move %OUTTEXTFILE% %INTEXTFILE% >nul

exit/b
